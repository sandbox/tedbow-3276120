<?php

namespace Drupal\project_installer\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\package_manager\Exception\StageException;
use Drupal\package_manager\Stage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InstallerForm extends FormBase {

  use StringTranslationTrait;

  /**
   * Constructs an InstallerForm object.
   *
   * @param \Drupal\package_manager\Stage $stage
   */
  public function __construct(Stage $stage) {
    $this->stage = $stage;
  }

  public static function create(ContainerInterface $container) {
    $stage = new Stage(
      $container->get('config.factory'),
      $container->get('package_manager.path_locator'),
      $container->get('package_manager.beginner'),
      $container->get('package_manager.stager'),
      $container->get('package_manager.committer'),
      $container->get('file_system'),
      $container->get('event_dispatcher'),
      $container->get('tempstore.shared'),
      $container->get('datetime.time')
    );
    return new static($stage);
  }


  public function getFormId() {
    return 'project_installer_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['project'] = [
      '#title' => $this->t('Project name'),
      '#type' => 'textfield',
      '#description' => $this->t('The Drupal project machine name.'),
      '#required' => TRUE,
    ];
    $form['install'] = [
      '#type' => 'submit',
      '#value' => $this->t('Install'),
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    Environment::setTimeLimit(500);
    $composer_package = 'drupal/' . $form_state->getValue('project');
    try {
      $this->stage->create();
      $this->stage->require([$composer_package => '*']);
      $this->stage->apply();
      $this->stage->destroy();
      $this->messenger()->addStatus($this->t('@project has been installed 🎉', ['@project' => $composer_package]));
    }
    catch (StageException $e) {
      $this->messenger()->addError($this->t('Sorry @project could not be installed 😭', ['@project' => $composer_package]));
    }

  }

}
